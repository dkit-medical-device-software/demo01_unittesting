#!/bin/sh

# This script shows the command to run the test:
# - normally you wouldn't need a script just to run tests.

python3 -m nose

# if everything ends up with OK then you're good
