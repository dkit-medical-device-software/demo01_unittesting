from exammarks import passes
from unittest import TestCase

# see: https://docs.python.org/3.5/library/unittest.html

class TestPassesFunction(TestCase):

    def test_grade_above_passmark_passes(self):
        self.assertTrue(passes(41.0))

    def test_grade_below_passmark_fails(self):
        self.assertFalse(passes(39.0))

    # this test is failing
    def test_grade_exactly_passmark_passes(self):
        self.assertFalse(passes(40.0))
